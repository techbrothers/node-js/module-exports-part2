module.exports.addition = (a, b) => {
  return `addition of 2 numbers ${ a + b}`;
};

module.exports.substraction = (a, b) => {
  return `substraction of 2 numbers ${ a - b}`;
};

module.exports.multiplication = (a, b) => {
  return `multiplication of 2 numbers ${ a * b}`;
};

module.exports.division = (a, b) => {
  return `division of 2 numbers ${ a / b}`;
};
