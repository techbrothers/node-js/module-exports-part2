var addition = (a, b) => {
  return `addition of 2 numbers ${ a + b}`;
};

var substraction = (a, b) => {
  return `substraction of 2 numbers ${ a - b}`;
};

var multiplication = (a, b) => {
  return `multiplication of 2 numbers ${ a * b}`;
};

var division = (a, b) => {
  return `division of 2 numbers ${ a / b}`;
};


module.exports.addition = addition;
module.exports.substraction = substraction;
module.exports.multiplication = multiplication;
module.exports.division = division;
